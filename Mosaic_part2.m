load imageData.mat

base_image = imresize(imread('IMG_20181212_164231810.jpg'), [128 NaN]);
indexes = zeros(size(base_image, 1), size(base_image, 2));

for i = 1:size(base_image, 1)
    for j = 1:size(base_image, 2)
        best_match = 1000;
        best_match_index = 0;
        pixel = int16(reshape(base_image(i, j, :), [1,3]));
        for k = 1:length(colours)
            p = int16(reshape(colours{k}, [1,3]));
            match = max(abs(p-pixel));
            
            if (match < best_match)
                best_match = match;
                best_match_index = k;
            end
        end
        indexes(i,j) = best_match_index;
    end
end