image1 = ones(size(indexes, 1), size(indexes, 2), 3);
full_image = ones(size(indexes, 1) * 64, size(indexes, 2) * 64, 3);

for i = 1:size(indexes, 1)
    for j = 1:size(indexes, 2)
        image1(i, j, :) = im2single(colours{indexes(i, j)});
        full_image(i*64-63:i*64, j*64-63:j*64, :) = im2single(photos{indexes(i, j)});
    end
end

imshow(image1)
figure
imshow(full_image)
