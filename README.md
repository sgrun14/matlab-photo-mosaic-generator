# MATLAB Photo Mosaic Generator

These MATLAB scripts are designed to generate a photo mosaic, when provided with a folder of jpg images and a base image.

To use them, run prepImages first, followed by mosaic part 2 and mosaic part 3.