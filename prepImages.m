function [images,colours] = prepImages()
%PREPIMAGES Prepare images for creating mosaic
%   This function takes all the images in the current folder and returns
%   64x64 pixel versions of them, along with the overall colour of each
%   image. It is designed to run best on multicore CPUs.

JPGfiles = dir('*.JPG');
numFiles = length(JPGfiles);
images = cell(1, numFiles);
colours = cell(1, numFiles);

parfor k = 1:numFiles
    I = imread(JPGfiles(k).name);
    s = size(I);
    s = s(1:2);
    d = min(s);
    
    cropped = imcrop(I, [0 0 d d]);
    images{k} = imresize(cropped, [64 64]);
    colours{k} = imresize(images{k}, [1 1]);
end
end

